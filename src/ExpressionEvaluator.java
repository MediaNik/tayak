import java.util.Scanner;

public class ExpressionEvaluator {
    private final ReversePolishNotationEncoder encoder = new ReversePolishNotationEncoder();
    private final ReversePolishNotationEvaluator evaluator = new ReversePolishNotationEvaluator();

    public double evaluate(String str){
        String rpn = encoder.infixToRPN(str);
        return evaluator.evaluateRPN(rpn.split(" "));
    }

    public static void main(String[] args) {
        String input = new Scanner(System.in).nextLine();
        System.out.println(new ExpressionEvaluator().evaluate(input));
    }
}

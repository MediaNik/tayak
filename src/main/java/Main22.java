import java.util.Scanner;

public class Main22 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int[][] ints = new int[N][];
        for (int i = 0; i < ints.length; i++) {
            int size = (i + 1) * 2;
            ints[i] = new int[size];
            for (int j = 0; j < ints[i].length; j++) {
                int k = j + 1;
                ints[i][j] = k;
                System.out.print(ints[i][j]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }
}

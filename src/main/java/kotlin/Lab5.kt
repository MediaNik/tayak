package ru.miet.lab1

import java.io.File

fun main(){
    val fileInfo = Informationer.ofOctetFile("C:\\Users\\rocto\\gdrive\\projects\\java\\LabsOtik\\3.txt")
    fileInfo.frequencies()
        .map { it.key to it.value }
        .sortedByDescending { it.second }
        .takeWhile { it.second >= 10 }
        .forEach { println("Octet ${java.lang.Long.toHexString(it.first)}'s frequency: ${it.second}") }
}
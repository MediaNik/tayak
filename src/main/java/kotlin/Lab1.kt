package ru.miet.lab1

import java.io.File
import kotlin.math.log2

fun main(){
    val fileInfo = Informationer.ofByteFile("C:\\Users\\rocto\\gdrive\\projects\\java\\LabsOtik\\text")
    // print info about all bytes
    println(fileInfo.length)
    println(fileInfo.fileInformation)
    for(i in -128..127){
        val info = fileInfo.informationOfElement(i.toByte())
        val frequency = fileInfo.frequencyOfElement(i.toByte())
        val probability = fileInfo.probabilityOfElement(i.toByte())
        if(info != 0.0) {
            println("Byte $i' info: $info, frequency: $frequency, probability: $probability")
        }
    }
    fileInfo.frequencies()
        .map { it.key to it.value }
        .sortedByDescending { it.second }
        .forEach { println("Byte ${it.first}'s frequency: ${it.second}") }
}

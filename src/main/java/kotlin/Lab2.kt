package ru.miet.lab1

fun main(){
    val fileInfo = Informationer.ofUnicodeFile("C:\\Users\\rocto\\gdrive\\projects\\java\\LabsOtik\\text")
    // print info about all bytes
    println(fileInfo.length)
    println(fileInfo.fileInformation)
    for(char in fileInfo.frequencies().keys){
        val info = fileInfo.informationOfElement(char)
        val frequency = fileInfo.frequencyOfElement(char)
        val probability = fileInfo.probabilityOfElement(char)
        if(info != 0.0) {
            println("Char $char' info: $info, frequency: $frequency, probability: $probability")
        }
    }
    fileInfo.frequencies()
        .map { it.key to it.value }
        .sortedByDescending { it.second }
        .forEach { println("Char ${it.first}'s frequency: ${it.second}") }
}
package ru.miet.lab1

import java.io.File
import kotlin.math.log2


interface Informationer<T>{
    val length: Int
    val fileInformation: Double
    fun frequencies(): Map<T, Int>
    fun frequencyOfElement(element: T): Int
    fun probabilityOfElement(element: T): Double
    fun informationOfElement(element: T): Double

    companion object{
        fun ofByteFile(filename: String): Informationer<Byte> {
            return InformationerImpl(File(filename).readBytes().toTypedArray())
        }
        fun ofUnicodeFile(filename: String): Informationer<Char> {
            return InformationerImpl(File(filename).readText().toCharArray().toTypedArray())
        }
        fun ofOctetFile(filename: String): Informationer<Long> {
            val bytes = File(filename).readBytes()
            val octets = arrayOfNulls<Long>(bytes.size / 8)
            for (i in octets.indices) {
                var octet = 0L
                for (j in 0..7) {
                    octet = octet or (bytes[i * 8 + j].toLong() shl (8 * (7 - j)))
                }
                octets[i] = octet
            }
            return InformationerImpl(octets as Array<Long>)
        }
    }
}

private class InformationerImpl<T>(data: Array<T>): Informationer<T> {
    override val length: Int
    private val frequencyMap: MutableMap<T, Int> = HashMap()
    private val probabilityMap: MutableMap<T, Double> = HashMap()
    override val fileInformation: Double

    override fun frequencies(): Map<T, Int> {
        return frequencyMap.toMap()
    }

    init {
        length = data.size
        for (value in data) {
            frequencyMap.merge(value, 1) { i1, i2 -> i1 + i2 }
        }
        var fileInformation = 0.0
        for((byte, frequency) in frequencyMap){
            probabilityMap[byte] = frequency.toDouble() / length
            fileInformation += informationOfElement(byte) * frequency
        }
        this.fileInformation = fileInformation
    }

    override fun informationOfElement(element: T): Double {
        val probability = probabilityOfElement(element)
        if(probability == 0.0){
            return 0.0
        }
        return -log2(probability)
    }

    override fun probabilityOfElement(element: T): Double {
        return probabilityMap[element] ?: 0.0
    }

    override fun frequencyOfElement(element: T): Int {
        return frequencyMap[element] ?: 0
    }
}
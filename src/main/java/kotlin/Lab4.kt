package ru.miet.lab1

import java.io.File

fun main(){
    val filesList = File("C:\\Users\\rocto\\gdrive\\projects\\java\\LabsOtik\\DifferentCodecs").listFiles()
    for(file in filesList){
        val fileInfo = Informationer.ofOctetFile(file.absolutePath)
        println("File ${file.name} info:")
        fileInfo.frequencies()
            .map { it.key to it.value }
            .sortedByDescending { it.second }
            .takeWhile { it.second >= 1000 }
            .forEach { println("Octet ${java.lang.Long.toHexString(it.first)}'s frequency: ${it.second}") }
    }
}
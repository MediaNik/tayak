//package lab3;
//
//import java.io.IOException;
//import java.nio.charset.StandardCharsets;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.util.*;
//
//public class Lab4 {
//    public static void main(String[] args) throws IOException {
//        List<String> strings = Files.readAllLines(Path.of("C:\\Users\\rocto\\gdrive\\projects\\java\\TAYAK\\parser.ebnf"), StandardCharsets.UTF_8);
//        char nextChar = 'a';
//        Map<Character, String> map = new HashMap<>();
//        Map<String, Character> reverseMap = new HashMap<>();
//        Map<Character, String> values = new HashMap<>();
//        Map<String, Character> reverseValues = new HashMap<>();
//        StringBuilder rules = new StringBuilder();
//        for (int i = 0; i < strings.size(); i++) {
//            String s = strings.get(i);
//            String[] split = s.trim().split("::=", 2);
//            String v1 = split[0].trim();
//            Character c = reverseMap.get(v1);
//            if(c == null){
//                c = nextChar++;
//                if(c == '|')
//                    c = nextChar++;
//                map.put(c, v1);
//                reverseMap.put(v1, c);
//            }
//            String v2 = split[1];
//            String[] values1 = v2.split("\\|");
//            StringBuilder output = new StringBuilder(c + ">");
//            for (String value : values1) {
//                String[] values2 = value.trim().split(" +");
//                for (String value2 : values2) {
//                    if(value2.startsWith("'") && value2.endsWith("'")){
//                        String value3 = value2.substring(1, value2.length() - 1);
//                        Character c1 = reverseValues.get(value3);
//                        if(c1 == null){
//                            c1 = nextChar++;
//                            if(c1 == '|')
//                                c1 = nextChar++;
//                            values.put(c1, value3);
//                            reverseValues.put(value3, c1);
//                        }
//                        output.append(c1);
//                    }else{
//                        Character c1 = reverseMap.get(value2);
//                        if(c1 == null){
//                            c1 = nextChar++;
//                            if(c1 == '|')
//                                c1 = nextChar++;
//                            map.put(c1, value2);
//                            reverseMap.put(value2, c1);
//                        }
//                        output.append(c1);
//                    }
//                }
//                output.append("|");
//            }
//            output.setLength(output.length() - 1);
//            rules.append(output).append("\n");
//        }
////        java.nio.file.Files.writeString(Path.of("C:\\Users\\rocto\\IdeaProjects\\untitled\\parser1.ebnf"), output1);
////        System.out.println(output1);
//
//        String s = java.nio.file.Files.readString(Path.of("C:\\Users\\rocto\\gdrive\\projects\\java\\TAYAK\\test.txt"));
//
//        List<String> tokens = splitToTokens(s);
//        StringBuilder inputCode = new StringBuilder();
//        for (String token : tokens) {
//            Character c = reverseValues.get(token);
//            if(c != null){
//                inputCode.append(c);
//            }else{
//                for(int i = 0; i < token.length(); i++){
//                    inputCode.append(reverseValues.get(token.charAt(i) + ""));
//                }
//            }
//        }
//        var storage = new Storage(Arrays.asList(rules.toString().split("\n")), map, values);
//        storage.checkLine(inputCode.toString());
////        System.out.println(output);
////        java.nio.file.Files.writeString(Path.of("C:\\Users\\rocto\\IdeaProjects\\untitled\\test1.txt"), output);
//
//    }
//
//    private static List<String> splitToTokens(String s) {
//        List<String> tokens = new ArrayList<>();
//        StringBuilder currentToken = new StringBuilder();
//        for (int i = 0; i < s.length(); i++) {
//            char c = s.charAt(i);
//            if(Character.isAlphabetic(c) || Character.isDigit(c)){
//                currentToken.append(c);
//            }else{
//                if(!currentToken.isEmpty()){
//                    tokens.add(currentToken.toString());
//                    currentToken.setLength(0);
//                }
//                if (c != ' ' && c != '\n' && c != '\r') {
//                    tokens.add(c + "");
//                }
//            }
//        }
//        if(!currentToken.isEmpty()){
//            tokens.add(currentToken.toString());
//        }
//        return tokens;
//    }
//}

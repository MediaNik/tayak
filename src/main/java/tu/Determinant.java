package tu;

import java.util.Scanner;

/**
 * @author Cris
 */
public class Determinant {
    public static void main(String[] args) {
        int a, b, c;
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter a: ");
        a = scanner.nextInt();
        System.out.println("enter b: ");
        b = scanner.nextInt();
        System.out.println("enter c: ");
        c = scanner.nextInt();
        double det = b * b - 4 * a * c;
        if (det > 0) {
            double x1 = (-b + Math.sqrt(det)) / (2 * a);
            double x2 = (-b - Math.sqrt(det)) / (2 * a);
            System.out.println("x1 = " + x1);
            System.out.println("x2 = " + x2);
        } else if (det == 0) {
            double x = -b / (2*a);
            System.out.println("x = " + x);
        } else {
            System.out.println("no roots");
        }


    }

}

package tu;

public class CycleFor {

    public static void main(String[] args) {
        /*
        for (в начале цикла; 1 шаг каждой итерации; 3 шаг каждой итерации){
            2 шаг каждой итерации;
        }
         */
        for (int i = 1; i<6; i = i + 1) {
            System.out.println(i);
        }
    }
}

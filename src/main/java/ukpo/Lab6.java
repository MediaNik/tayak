package ukpo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Human{
    public int passportNumber;
    public String lastName;

    public Human(int passportNumber, String lastName) {
        this.passportNumber = passportNumber;
        this.lastName = lastName;
        if (this.passportNumber >= 10_000_000 || this.passportNumber < 1_000_000) {
            throw new IllegalArgumentException("Паспорт должен быть семизначным");
        }
        if(lastName.length() > 20){
            throw new IllegalArgumentException("Фамилия должна быть не длиннее двадцати символов");
        }
    }

    @Override
    public String toString() {
        return "Человек " + lastName + ", паспорт " + passportNumber;
    }
}

class Visitor extends Human {
    public String hotelName;
    public int daysCount;
    public Visitor(int passportNumber, String lastName, String hotelName, int daysCount) {
        super(passportNumber, lastName);
        this.hotelName = hotelName;
        if(this.hotelName.length() > 15){
            throw new IllegalArgumentException("Наименование отеля должно быть не длиннее 15 символов");
        }
        this.daysCount = daysCount;
    }
    public boolean livesInHotel(String hotelName){
        return this.hotelName.equals(hotelName);
    }

    public boolean livesMoreThan(int maxDays) {
        return maxDays > this.daysCount;
    }

    public boolean livesEqualTo(int days){
        return this.daysCount == days;
    }
}

public class Lab6 {
    public static void main(String[] args) {
        int condition;
        List<Visitor> visitors = new ArrayList<>();
        do{
            System.out.println("Введите проживающего в отеле(номер паспорта, фамилия, наименование отеля, количество суток проживания, всё с новой строки): ");
            Scanner scanner = new Scanner(System.in);
            int passportNumber = Integer.parseInt(scanner.nextLine());
            String lastName = scanner.nextLine();
            String hotelName = scanner.nextLine();
            int daysCount = Integer.parseInt(scanner.nextLine());
            Visitor visitor = new Visitor(passportNumber, lastName, hotelName, daysCount);
            visitors.add(visitor);
            System.out.println("Продолжить заполнение проживающих? (1 - да, 0 - нет)");
            condition = scanner.nextInt();
        }while(condition == 1);

        int maxDays = 0;
        for (Visitor visitor : visitors) {
            if(visitor.livesInHotel("Загородный") && visitor.livesMoreThan(maxDays)){
                maxDays = visitor.daysCount;
            }
        }
        for (Visitor visitor : visitors) {
            if(visitor.livesInHotel("Загородный") && visitor.livesEqualTo(maxDays)){
                System.out.println(visitor);
            }
        }
    }
}
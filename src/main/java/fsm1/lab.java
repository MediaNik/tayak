package fsm1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Link {
    char s;
    String inp;
    String stack;
    int index;
    boolean term;

    Link(char s, String inp, String stack, boolean t) {
        this.s = s;
        this.inp = inp;
        this.stack = stack;
        this.index = -1;
        this.term = t;
    }

    Link(char s, String inp, String stack) {
        this.s = s;
        this.inp = inp;
        this.stack = stack;
        this.index = -1;
    }
}

class Fargs {
    char s;
    char p;
    char h;

    Fargs(char s, char p, char h) {
        this.s = s;
        this.p = p;
        this.h = h;
    }
}

class Value {
    char s;
    String c;

    Value(char s, String c) {
        this.s = s;
        this.c = c;
    }
}

class Command {
    Fargs f;
    List<Value> values;

    Command(Fargs f, List<Value> v) {
        this.f = f;
        this.values = v;
    }
}

class Storage {

    private File file;
    private Set<Character> P = new HashSet<>();
    private Set<Character> H = new HashSet<>();
    private char s0 = '0';
    private char h0 = '|';
    private char emptySymbol = '\0';

    private List<Command> commands = new ArrayList<>();
    private List<Link> chain = new ArrayList<>();

    Storage(String filename) throws FileNotFoundException {
        this.file = new File(filename);
        Scanner sc = new Scanner(file);
        String tmpStr;
        Pattern p = Pattern.compile("(.)>(.+)");
        while (sc.hasNextLine()) {
            tmpStr = sc.nextLine();
            if (tmpStr.isEmpty()) {
                continue;
            }
            Matcher m = p.matcher(tmpStr);
            if (!m.matches() || tmpStr.charAt(tmpStr.length()-1) == '|'
                || tmpStr.charAt(2) == '|') {
                throw new RuntimeException("Invalid string format");
            } else {
                H.add(m.group(1).charAt(0));
                Fargs fargs = new Fargs(s0, emptySymbol, m.group(1).charAt(0));
                List<Value> values = new ArrayList<>();
                values.add(new Value(s0, ""));
                commands.add(new Command(fargs, values));

                for (int i = 0; i < m.group(2).length(); i++) {
                    char c = m.group(2).charAt(i);
                    if (c == '|') {
                        if (m.group(2).charAt(i-1) != '|') {
                            values.add(new Value(s0, ""));
                        }
                    } else {
                        P.add(c);
                        int index = values.size() - 1;
                        values.get(index).c += c;
                    }
                }

                for (Value value : values) {
                    StringBuilder sb = new StringBuilder(value.c);
                    value.c = sb.reverse().toString();
                }

            }
        }

        for (Character c : H) {
            P.remove(c);
        }

        for (Character c : P) {
            List<Value> vals = new ArrayList<>();
            vals.add(new Value(s0, "\0"));
            commands.add(new Command(new Fargs(s0, c, c), vals));
        }

        List<Value> lastVals = new ArrayList<>();
        lastVals.add(new Value(s0, "\0"));
        commands.add(new Command(new Fargs(s0, emptySymbol, h0), lastVals));

    }

    void showInfo() {
        System.out.println("Входной алфавит:");
        System.out.print("P = {");
        for (Character c : P) {
            System.out.print(c + ", ");
        }
        System.out.println("\b\b}");

        System.out.println("\nАлфавит магазинных символов:");
        System.out.print("Z = {");
        for (Character c : H) {
            System.out.print(c + ", ");
        }
        for (Character c : P) {
            System.out.print(c + ", ");
        }
        System.out.println("h0}");

        System.out.println("\nСписок команд:");
        for (Command cmd : commands) {
            System.out.print("f(s" + cmd.f.s + ", ");
            if (cmd.f.p == emptySymbol) {
                System.out.print("lambda");
            } else {
                System.out.print(cmd.f.p);
            }
            System.out.print(", ");
            if (cmd.f.h == h0) {
                System.out.print("h0");
            } else {
                System.out.print(cmd.f.h);
            }
            System.out.print(") = {");

            for (Value v : cmd.values) {
                System.out.print("(s" + v.s + ", ");
                if (v.c.charAt(0) == emptySymbol) {
                    System.out.print("lambda");
                } else {
                    System.out.print(v.c);
                }
                System.out.print("); ");
            }
            System.out.println("\b\b}");
        }
    }

    void showChain() {
        System.out.println("\nЦепочка конфигураций: ");
        for (Link link : chain) {
            System.out.print("(s" + link.s + ", ");
            if (link.inp.isEmpty()) {
                System.out.print("lambda");
            } else {
                System.out.print(link.inp);
            }
            System.out.print(", h0" + link.stack + ") | ");
        }
        System.out.println("(s0, lambda, lambda)");
    }

    boolean pushLink() {
        int chSize = chain.size();
        int stackSize;
        int i;
        for (i = 0; i < commands.size(); i++) {
            stackSize = chain.get(chSize - 1).stack.length();
            if (!chain.get(chain.size()-1).inp.isEmpty()
                && !chain.get(chain.size() - 1).stack.isEmpty()
                && chain.get(chSize-1).s == commands.get(i).f.s
                && (chain.get(chSize-1).inp.charAt(0) == commands.get(i).f.p
                || emptySymbol == commands.get(i).f.p)
                && chain.get(chSize-1).stack.charAt(stackSize-1) == commands.get(i).f.h) {

                for (int j = 0; j < commands.get(i).values.size(); j++) {
                    showChain();
                    chain.add(new Link(commands.get(i).values.get(j).s,
                        chain.get(chSize-1).inp, chain.get(chSize-1).stack));

                    if (commands.get(i).f.p != emptySymbol) {
                        StringBuilder sb = new StringBuilder(chain.get(chSize).inp);
                        sb.reverse();
                        sb.deleteCharAt(sb.length()-1);
                        chain.get(chSize).inp = sb.reverse().toString();
                    }

                    chain.get(chSize).stack = chain.get(chSize).stack.substring(0, stackSize-1);
                    if(!commands.get(i).values.get(j).c.equals("\0")) {
                        chain.get(chSize).stack += commands.get(i).values.get(j).c;
                    }

                    if (chain.get(chSize).inp.length() < chain.get(chSize).stack.length()) {
                        chain.remove(chain.size()-1);
//                        chain.remove(chain.size()-1);
//                        return false;
                    } else {
                        if (chain.get(chain.size()-1).inp.isEmpty()
                            && chain.get(chain.size()-1).stack.isEmpty()
                            || pushLink()) {
                            return true;
                        }
                    }
                }
            }
        }

        if (i == commands.size()) {
            chain.remove(chain.size()-1);
            return false;
        }

        return true;
    }

    boolean checkLine(String str) {
        if (commands.get(0).values.size() == 1) {
            chain.add(new Link(s0, str, "", false));
        } else {
            chain.add(new Link(s0, str, "", true));
        }

        chain.get(0).stack += commands.get(0).f.h;

        boolean res = pushLink();
        if (res) {
            System.out.println("String accepted");
//            showChain();
        } else {
            System.out.println("String rejected");
//            showChain();
        }
        chain.clear();
        return res;
    }

}

class Main {

    public static void main(String[] args) throws FileNotFoundException {
        Storage storage = new Storage("C:\\Users\\rocto\\gdrive\\projects\\java\\TAYAK\\file.txt");

        // Use storage object
        storage.showInfo();
        while(true){
            System.out.println("Введите строчку");
            String line = new Scanner(System.in).nextLine();
            storage.checkLine(line);
        }
    }
}
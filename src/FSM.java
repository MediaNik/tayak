import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class State {
    String name;
    List<Transition> transitions;

    State(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        State state = (State) o;
        return Objects.equals(name, state.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}

class Transition {
    State from;
    char input;
    State to;

    Transition(State from, char input, State to) {
        this.from = from;
        this.input = input;
        this.to = to;
    }
}

public class FSM {

    State startState;
    Map<Integer, State> states;
    List<Transition> transitions;

    FSM(String filename) {
        states = new HashMap<>(100);
        transitions = new ArrayList<>(100);

        try {
            Scanner scanner = new Scanner(new File(filename));
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                parseTransition(line);
            }
            scanner.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        startState = states.get(0);
    }

    void parseTransition(String line) {
        Pattern pattern = Pattern.compile("q(\\d+),(.)=([qf])(\\d+)");
        Matcher matcher = pattern.matcher(line);

        if (matcher.matches()) {
            int fromIndex = Integer.parseInt(matcher.group(1));
            char input = matcher.group(2).charAt(0);
            int toIndex = Integer.parseInt(matcher.group(4));

            State fromState = getOrCreateState(fromIndex);
            State toState = getOrCreateState(toIndex);

            Transition trans = new Transition(fromState, input, toState);
            transitions.set(transitions.size() - 1, trans);
        }
    }

    State getOrCreateState(int index) {
        if (states.get(index) == null) {
            states.put(index, new State("q" + index));
        }
        return states.get(index);
    }



    public static void main(String[] args) {
        FSM fsm = new FSM("C:\\Users\\rocto\\gdrive\\projects\\java\\TAYAK\\lab2\\transitions.txt");
    }

    boolean processInputString(String input) {
        State current = startState;

        for(char c : input.toCharArray()) {
            Transition found = null;
            for(Transition t : transitions) {
                if (t.from == current && t.input == c) {
                    found = t;
                    break;
                }
            }

            if (found == null) {
                return false; // reject
            }

            current = found.to;
        }

        return true; // accept
    }

    // Check if FSM is deterministic
    boolean checkDeterministic() {
        for(State s : states.values()) {
            boolean[] inputsSeen = new boolean[128];

            for(Transition t : transitions) {
                if (t.from == s) {
                    if (inputsSeen[t.input]) {
                        return false; // nondeterministic
                    }
                    inputsSeen[t.input] = true;
                }
            }
        }

        return true; // deterministic
    }

    // Print transition table
    void printTransitions() {
        System.out.println("Transition table:");

        for (Transition t : transitions) {
            System.out.println(t.from.name + "," + t.input + "->" + t.to.name);
        }
    }


}
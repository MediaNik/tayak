package org.example;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    static class Pair<T, V>{
        T first;
        V second;

        public Pair(T first, V second) {
            this.first = first;
            this.second = second;
        }

        @Override
        public String toString() {
            return first + "," + second;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Pair<?, ?> pair = (Pair<?, ?>) o;
            return Objects.equals(first, pair.first) && Objects.equals(second, pair.second);
        }

        @Override
        public int hashCode() {
            return Objects.hash(first, second);
        }
    }

    static Map<Pair<String, Character>, Set<String>> ndTable = new HashMap<>();
    static Map<Pair<String, Character>, String> dTable = new HashMap<>();

    static List<String> unusedStates = new ArrayList<>();
    static Set<String> usedStates = new HashSet<>();

    static Set<Character> alphabet = new HashSet<>();

    static String makeState(Set<String> states) {
        StringBuilder name = new StringBuilder();
        for (String state : states) {
            name.append(state);
        }
        for (Character symbol : alphabet) {
            for (String state : states) {
                Pair<String, Character> pair = new Pair<>(state, symbol);
                if (ndTable.containsKey(pair)) {
                    Set<String> nextStates = new HashSet<>(ndTable.get(pair));
                    nextStates.add(state);
                    ndTable.put(new Pair<>(name.toString(), symbol), new HashSet<>(nextStates));
                }
            }
        }
        return name.toString();
    }

    static void printTable(Map<Pair<String, Character>, String> table) {
        for (Map.Entry<Pair<String, Character>, String> entry : table.entrySet()) {
            Pair<String, Character> key = entry.getKey();
            String value = entry.getValue();
            System.out.println(key.first + "," + key.second + "=" + value);
        }
    }

    static void determinize() {
        while (!unusedStates.isEmpty()) {
            String current = unusedStates.remove(0);

            for (Character symbol : alphabet) {
                Pair<String, Character> pair = new Pair<>(current, symbol);
                if (!ndTable.containsKey(pair)) continue;

                Set<String> nextStates = ndTable.get(pair);
                String newState = makeState(nextStates);
                if (!usedStates.contains(newState)) {
                    unusedStates.add(newState);
                }
                dTable.put(pair, newState);
                usedStates.add(current);
            }
        }
    }

    static void parseAutomaton(String fileName) throws FileNotFoundException {
        File file = new File(fileName);
        try(Scanner sc = new Scanner(file);) {

            while (sc.hasNextLine()) {
                String line = sc.nextLine();

                Pattern pattern = Pattern.compile("q(\\d+),(.)=([qf])(\\d+)");
                Matcher matcher = pattern.matcher(line);

                if (matcher.matches()) {
                    String startState = 'q' + matcher.group(1);
                    Character symbol = matcher.group(2).charAt(0);
                    String nextState = matcher.group(3) + matcher.group(4);
                    ndTable.computeIfAbsent(new Pair<>(startState, symbol), v -> new HashSet<>()).add(nextState);
                    unusedStates.add(startState);
                    alphabet.add(symbol);
                }else{
                    throw new MyOwnException("Неправильный формат у строки: " + line);
                }

            }
        }

        boolean isDeterministic = true;
        for (Set<String> states : ndTable.values()) {
            if (states.size() > 1) {
                isDeterministic = false;
                break;
            }
        }

        if (isDeterministic) {
            for (Map.Entry<Pair<String, Character>, Set<String>> entry : ndTable.entrySet()) {
                dTable.put(entry.getKey(), entry.getValue().iterator().next());
            }
        } else {
            determinize();
            printTable(dTable);
        }

    }

    static int parseString(String str) {
        String currentState = "q0";

        for (int i = 0; i < str.length(); i++) {
            Character symbol = str.charAt(i);
            Pair<String, Character> pair = new Pair<>(currentState, symbol);

            if (!dTable.containsKey(pair)) {
                return -1;
            }

            currentState = dTable.get(pair);
            if (currentState.contains("f") && i == str.length() - 1) {
                return 0;
            } else if (!currentState.contains("f") && i == str.length() - 1) {
                return -1;
            }
        }

        return -1;
    }

    public static void main(String[] args) throws FileNotFoundException {
        try {
            parseAutomaton("C:\\Users\\rocto\\IdeaProjects\\untitled\\untitled.txt");
        }catch (MyOwnException e){
            System.err.println(e.getMessage());
        }

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            int result = parseString(line);
            if (result < 0) {
                System.out.println("Not a valid string");
            } else {
                System.out.println("Valid string");
            }
        }
        scanner.close();
    }
}
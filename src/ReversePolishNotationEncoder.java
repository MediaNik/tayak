import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

public class ReversePolishNotationEncoder {
    public static final Set<String> functions = Set.of("log");
    private static final Map<String, Integer> precedenceMap = createPrecedenceMap();
    public String infixToRPN(String expression) {
        StringBuilder result = new StringBuilder();
        Stack<String> operatorStack = new Stack<>();

        char[] charArray = expression.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            char c = charArray[i];
            if (Character.isWhitespace(c)) {
                continue;
            }
            if (Character.isDigit(c) || c == '.') {
                // If the character is a digit, append it to the result.
                result.append(c);
                continue;
            }else{
                if(Character.isDigit(result.charAt(result.length() - 1))){
                    result.append(' ');
                    try{
                        String[] s = result.toString().split(" ");
                        Double.parseDouble(s[s.length - 1]);
                    }catch (NumberFormatException e){
                        throw new IllegalArgumentException("Illegal number at index " + (i));
                    }
                }
            }
            if (c == '(') {
                // If an open parenthesis is encountered, push it onto the operator stack.
                operatorStack.push(c + "");
            } else if (c == ')') {
                // If a close parenthesis is encountered, pop operators from the stack and append them to the result
                // until an open parenthesis is found.
                while (!operatorStack.isEmpty() && precedenceMap.get(operatorStack.peek()) != 0) {
                    try {
                        result.append(operatorStack.pop()).append(' ');
                    }catch (EmptyStackException e){
                        throw new IllegalArgumentException("Illegal closing parenthesis at index " + (i+1));
                    }
                }
                String op = operatorStack.pop(); // Pop the open parenthesis.
                if(!op.equals("("))
                    result.append(op).append(' ');
            }else if(c == ','){
                while(!operatorStack.isEmpty()
                    && !operatorStack.peek().equals("log")){
                    result.append(operatorStack.pop()).append(' ');
                }
            } else if (isOperator(c)) {
                // The character is an operator or a comma (for function arguments).
                while (canPop(operatorStack, c)) {
                    // Pop operators from the stack and append them to the result if they have higher or equal precedence.
                    result.append(operatorStack.pop()).append(' ');
                }
                operatorStack.push(c + "");
            } else if (Character.isLetter(c)) {
                // The character is a letter, indicating a function.
                StringBuilder functionName = new StringBuilder();
                functionName.append(c);

                // Continue to build the function name until a non-letter character is encountered.
                while ((c = getNextChar(expression, ++i)) != 0 && Character.isLetterOrDigit(c)) {
                    functionName.append(c);
                }

                // Push the function name onto the operator stack.
                operatorStack.push(functionName.toString());
                if(!functions.contains(operatorStack.peek())){
                    throw new IllegalArgumentException("Incorrect input: invalid function at index " + (i+1));
                }
                while(Character.isWhitespace(c)){
                    c = getNextChar(expression, ++i);
                }
                if(c != '('){
                    throw new IllegalArgumentException("Incorrect input: no opening parenthesis after function input at index " + (i+1));
                }
            }
        }

        if(operatorStack.contains("(")){
            throw new IllegalArgumentException("Unmatching parenthesis");
        }
        while (!operatorStack.isEmpty()) {
            // Pop any remaining operators from the stack and append them to the result.
            result.append(operatorStack.pop()).append(' ');
        }

        return result.toString();
    }

    private static boolean canPop(Stack<String> operatorStack, char c) {
        return !operatorStack.isEmpty() &&
            !operatorStack.peek().equals("(")
            && precedenceMap.get(c + "") <= precedenceMap.get(operatorStack.peek());
    }

    private static Map<String, Integer> createPrecedenceMap() {
        Map<String, Integer> map = new HashMap<>();
        map.put("+", 1);
        map.put("-", 1);
        map.put("*", 2);
        map.put("/", 2);
        map.put("(", 0);
        for (String function : functions) {
            map.put(function, 0);
        }
        return map;
    }

    private static boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    private static char getNextChar(String expression, int i) {
        if(expression.length() > i){
            return expression.charAt(i);
        }
        return 0;
    }

    public static void main(String[] args) {
        ReversePolishNotationEncoder reversePolishNotationEncoder = new ReversePolishNotationEncoder();
        String infixExpression = "33.33++log(4+2*3,2)*(2-1)";
        String rpnExpression = reversePolishNotationEncoder.infixToRPN(infixExpression);
        System.out.println("Infix Expression: " + infixExpression);
        System.out.println("RPN Expression: " + rpnExpression);
    }
}
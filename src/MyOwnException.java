package org.example;

public class MyOwnException extends RuntimeException {
    public MyOwnException(String s) {
        super(s);
    }
}

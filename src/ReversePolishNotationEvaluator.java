import java.util.EmptyStackException;
import java.util.Stack;

public class ReversePolishNotationEvaluator {
    public double evaluateRPN(String[] tokens) {
        Stack<Double> stack = new Stack<>();

        for (String token : tokens) {
            if (isNumeric(token)) {
                stack.push(Double.parseDouble(token));
            } else if (isFunction(token)) {
                try {
                    double arg2 = stack.pop();
                    double arg1 = stack.pop();
                    double result = evaluateFunction(token, arg1, arg2);
                    stack.push(result);
                }catch (EmptyStackException e){
                    throw new IllegalArgumentException("Illegal operation: " + token);
                }
            } else {
                try {
                    double operand2 = stack.pop();
                    double operand1 = stack.pop();
                    double result = performOperation(token, operand1, operand2);
                    stack.push(result);
                }catch (EmptyStackException e){
                    throw new IllegalArgumentException("Illegal operation: " + token);
                }
            }
        }

        return stack.pop();
    }

    private static boolean isNumeric(String token) {
        try {
            Double.parseDouble(token);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static boolean isFunction(String token) {
        return ReversePolishNotationEncoder.functions.contains(token);
    }

    private double evaluateFunction(String functionName, double arg1, double arg2) {
        if (functionName.equals("log")) {
            if(arg1 == 1 || arg1 <= 0 || arg2 <= 0){
                throw new IllegalArgumentException("Illegal arguments for log: log(" + arg1 + "," + arg2 + ")");
            }
            return (Math.log(arg2) / Math.log(arg1));
        } else if (functionName.equals("pow")) {
            double value = Math.pow(arg1, arg2);
            if(Double.isNaN(value) || Double.isInfinite(value)){
                throw new IllegalArgumentException("Expression results either in infinities or NaNs: " + arg1 + "^" + arg2);
            }
            return value;
        }else {
            throw new IllegalArgumentException("Unsupported function: " + functionName);
        }
    }

    private double performOperation(String operator, double operand1, double operand2) {
        return switch (operator) {
            case "+" -> operand1 + operand2;
            case "-" -> operand1 - operand2;
            case "*" -> operand1 * operand2;
            case "/" -> {
                if (operand2 == 0) {
                    throw new IllegalArgumentException("Illegal arguments for division: " + operand1 + "/" + operand2);
                }
                yield operand1 / operand2;
            }
            default -> throw new IllegalArgumentException("Unsupported operator: " + operator);
        };
    }

}
